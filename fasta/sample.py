#!/usr/bin/env python3

'''
USAGE
    fasta_sample [OPTION] [FILE...]

DESCRIPTION
    Sample sequences from FASTA files.
    
OPTIONS
    -n, --num=REGEX
        Number of sequence to be sampled. Default=1

    --help
        Display this message

'''

import getopt, sys, fileinput, fasta, re, random

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "n:", 
                                       ['num=', 'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-n', '--num'):
                self['n'] = int(float(a))

        self.args = args
    
    def set_default(self):
    
        # default parameter value
        self["n"] = 1

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args
    
    # load input sequences
    sequences = list(fasta.reader(fileinput.input()))
    N = len(sequences)
    
    # stop here if there is no sequence in input
    if N == 0:
        return 0

    n = options["n"] if options["n"] <= N else N

    # sample sequences and write them in the output
    if n < 1:
        raise ValueError(f"n (={n}) must be a positive integer.")
    for name, sequence in random.sample(sequences, k=n):
        fasta.writer(name, sequence, sys.stdout)

    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())

