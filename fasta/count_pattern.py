#!/usr/bin/env python3

'''
USAGE
    fasta_count_pattern.py [OPTION] PATTERN [FILE...]

DESCRIPTION
    Count the occurence(s) of the PATTERN in each of the input 
    sequences. Returns the name of the sequences along with the number
    of matches. Ignores gaps.
    
OPTIONS
    --help
        Display this message

'''

import getopt, sys, fileinput, fasta, re

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "", ['help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)

        self["pattern"] = re.compile(args.pop(0))
        self.args = args

    def set_default(self):
    
        # default parameter value
        pass

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    pattern = options["pattern"]
    sys.argv[1:] = options.args
    
    # search and count patterns in input sequences
    for name, sequence in fasta.reader(fileinput.input()):
        n = len(pattern.findall(sequence))
        sys.stdout.write(f"{name}\t{n}\n")
    
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())

