#!/usr/bin/env python3

import textwrap, re

SEQ = re.compile("\S+")

class SeqRange(object):
    
    # This pattern returns an empty match if nothing is found
    pattern = re.compile(r"(?P<start>[0-9]+)?(?P<dash>-)?(?P<end>[0-9]+)?")

    def __init__(self, expr):
        self.start = None
        self.end = None
        self._match = SeqRange.pattern.match(expr)
        if not matched_a_range(self._match):
            raise ValueError(f"unmatched range pattern: {expr}")
        start, dash, end = self._match.groups()
        self.start = start
        self.end = end
        self._single_position = dash is None

    def seq_slice(self, sequence):
        return sequence[self.slice]

    def al_slice(self, alignment):
        return ( (name, sequence[self.slice]) 
                 for name, sequence in alignment )

    @property
    def slice(self):
        if self._single_position:
            return slice(self.start-1, self.start)
        elif self.start is None:
            return slice(0, self.end)
        else:
            return slice(self.start-1, self.end)

    @property
    def start(self):
        return self._start

    @start.setter
    def start(self, value):
        if value is not None:
            value = int(value)
            if value < 1:
                raise ValueError("position values must be greater than 1")
            if self.end is not None and value > self.end:
                raise ValueError("start position must preceed the end position")
        self._start = value

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, value):
        if value is not None:
            value = int(value)
            if value < 1:
                raise ValueError("position values must be greater than 1")
            if self.start is not None and value < self.start:
                raise ValueError("end position must follow the start position")
        self._end = value

    def __contains__(self, item):
        if self._single_position:
            return item == self.start
        if self.start is None:
            return item <= self.end
        if self.end is None:
            return item >= self.start
        return self.start <= item <= self.end

class EukariotGeneticCode(dict):
    '''translate codon into amino acid according eukariotic genetic code'''
    
    def __init__(self, value={}, allow_ambiguous=False):
        dict.__init__(value)
        codons = []
        for first in ('T','C','A','G') :
            for second in ('T','C','A','G') :
                for third in ('T','C','A','G'):
                    codons += [first + second + third]
        AA = ['F','F','L','L','S','S','S','S',
              'Y','Y','*','*','C','C','*','W',
              'L','L','L','L','P','P','P','P',
              'H','H','Q','Q','R','R','R','R',
              'I','I','I','M','T','T','T','T',
              'N','N','K','K','S','S','R','R',
              'V','V','V','V','A','A','A','A',
              'D','D','E','E','G','G','G','G']
        self.update(dict(zip(codons, AA)))
        self.update(value)
        self.allow_ambiguous=allow_ambiguous

    def __getitem__(self, codon):
        if 'N' in codon or '?' in codon: 
            return 'X'
        elif codon == '---': 
            return '-'
        elif '-' in codon or len(codon) < 3:
            raise KeyError(f"incomplete codon: {repr(codon)}")
        try:
            return dict.__getitem__(self, codon)
        except KeyError:
            if self.allow_ambiguous:
                return "X"
            else:
                raise ValueError(f"unknown codon: {repr(codon)}")

def reverse_complement(seq):
    A, T, C, G = map(ord, "ATGC")
    complement = {
        A: T,
        T: A,
        C: G,
        G: C
    }
    return seq.translate(complement)[::-1]

def reader(f):
    header, sequence = "", ""
    for line in f:
        if line.startswith(">"): 
            if header and sequence:
                yield header, sequence
                sequence = ""
            header = line.strip()[1:]
        else:
            sequence += "".join(SEQ.findall(line))
    if header and sequence:
        yield header, sequence

def writer(header, sequence, fout):
    wrap = textwrap.wrap(sequence, 60, break_on_hyphens=False)
    sequence = "\n".join(wrap)
    fout.write(f">{header}\n{sequence}\n")

def matched_a_range(match):
        return not match.groups() in ((None, None, None), (None, "-", None))

def is_range(expr):
    return matched_a_range(SeqRange.pattern.match(expr))

def read_range(expr):
    return SeqRange(expr)

def read_ranges(exprs):
    return [ SeqRange(expr) for expr in exprs.split(",") ]
