#!/usr/bin/env python3

'''
USAGE
    fasta_extract [OPTION] PATTERN [FILE...]

DESCRIPTION
    Extract the matching PATTERN in input sequences. Option -t allows to
    combine sequences with topological information, whose match 
    coordinates will be reported on input sequences (names must be
    identical). Option -a specified that the input sequences are 
    aligned, and therefore gap will be ignored. In default mode, using 
    the topological reference, sequences that are not listed in the 
    topological reference are skipped. 
    
OPTIONS
    -a, --alignment-mode
        Input is aligned, gap will be ignored.

    -t, --topological-reference=FILE
        Use sequences in the FILE as a topological reference. Names must
        match with the input. In alignment mode (option -a), only the
        first sequence of the FILE will be used.

    --help
        Display this message

'''

import getopt, sys, fileinput, fasta, re
from itertools import accumulate

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "at:", 
                                       ['alignment-mode', 
                                        'topological-reference=',
                                        'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-a', '--alignment-mode'):
                self['alignment_mode'] = True
            elif o in ('-t', '--topological-reference'):
                self['topological_reference'] = a

        self["pattern"] = re.compile(args.pop(0))
        self.args = args
    
    def set_default(self):
    
        # default parameter value
        self['alignment_mode'] = False
        self['topological_reference'] = None

def only_gaps(sequences, i):
    return all( sequence[i] == "-" for _, sequence in sequences )

def alignment_len(sequences):
    return len(sequences[0][1])

def aligned(sequences):
    l = alignment_len(sequences)
    return all( len(s) == l for _, s in sequences )

def load_alignment(f):
    sequences = list(fasta.reader(f))
    if not aligned(sequences):
        raise ValueError("input sequences are not aligned.")
    return sequences

def get_match_coordinates(sequence, pattern):
    m = pattern.search(sequence)
    return (None, None) if m is None else m.span()

def extract_aligned_sequence_coordinates(sequence, start, end, reference):
    unaligned_site_indexes = ( i-1 for i in accumulate( 0 if site_ == "-" else 1 
                                                        for site_ in reference ))
    site_indexes = list(enumerate(unaligned_site_indexes))
    extracted_sequence =  "".join( sequence[al_i] 
                                   for al_i, unal_i in site_indexes 
                                   if start <= unal_i < end )
    return extracted_sequence

def alignment_mode(pattern, topological_reference=None):
    
    # load input sequences
    sequences = load_alignment(fileinput.input())
    output = []

    # takes the first sequence found in the topological reference if a
    # reference is provided
    if topological_reference is not None:
        with open(topological_reference) as f:
            for name, sequence in fasta.reader(f):
                start, end = get_match_coordinates(sequence, pattern)
                reference = dict(sequences)[name]
                break
        
    # otherwise takes the first sequence from the input
    else:
        reference = sequences[0][1]
        start, end = get_match_coordinates(reference)

    # process input sequences
    for name, sequence in sequences:
        sequence = extract_aligned_sequence_coordinates(
            sequence, start, end, reference)
        output.append((name, sequence))
    
    # returns 0 if everything succeeded
    return output

def default_mode(pattern, topological_reference=None):
    
    # reads the topological reference if provided
    if topological_reference is not None:
        with open(topological_reference) as f:
            coordinates = dict(
                (name, get_match_coordinates(sequence, pattern))
                for name, sequence in fasta.reader(f)
            )

        # process input sequences
        for name, sequence in fasta.reader(fileinput.input()):
            try:
                start, end = coordinates[name]
            except KeyError:
                continue
            yield name, sequence[start:end]

    # otherwise coordinates will be extracted from the input
    else:
        for name, sequence in fasta.reader(fileinput.input()):
            start, end = get_match_coordinates(sequence, pattern)
            yield name, sequence[start:end]

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args

    # In alignment mode, extract the aligment columns corresponding to the match
    # in the topological reference.
    if options["alignment_mode"]:

        # Load output sequences
        sequences = list(alignment_mode(
            options["pattern"],
            options["topological_reference"]
        ))

        # Trim the trailing gaps (algorithmic artifact)
        names = ( name for name, _ in sequences )
        cols = list(zip(*( sequence for _, sequence in sequences )))
        
        # Search backward for the first column with something else than gaps 
        i = len(cols)
        while i > 0:
            i -= 1
            if not all( site == "-" for site in cols[i] ):
                break
            
        # Restaure the sequences after trimming the trailing gaps and print
        # it out
        for name, sequence in zip(names, zip(*cols[:i])):
            fasta.writer(name, "".join(sequence), sys.stdout)

    # Default mode
    else:

        for name, sequence in default_mode(
            options["pattern"],
            options["topological_reference"]
            ):
            fasta.writer(name, sequence, sys.stdout)

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())
