#!/usr/bin/env python3

'''
USAGE
    fasta_rename.py [OPTION] TABLE [FILE...]

DESCRIPTION
    Replace header in the input FASTA file using a correspondance table.

    TABLE file must list correspondances separated by a tabulation in 
    the following order:
    
    <old name>TAB<new name>
    ...
    
OPTIONS
    -f, --format=[PREFIX]:[DIGITS]
        If option -m is selected, set up the new name format. PREFIX 
        can be any string. DIGITS specifies the number of digit (with
        leading 0) to save the index of the sequence in the input. The 
        column character is only used to separate the field and will 
        not appear in the new names. Default=seq:5.

    -i, --start-index=INT
        If option -m is selected, set up the starting index number. 
        Default=1.

    -m, --make-new-names
        Instead of replacing the name in the FASTA file using the 
        correspondance TABLE, make up new names and write the table. 
        By default, these names will be formatted as [prefix][digits].
        Format can be customized with option -f.

    --help
        Display this message

'''

import getopt, sys, fileinput, fasta

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "f:i:m",
                ['format=', 'start-index=', 'make-new-names', 'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-f', '--format'):
                self["format"] = a
            elif o in ('-i', '--start-index'):
                self["start_index"] = int(a)
            elif o in ('-m', '--make-new-names'):
                self["make_new_names"] = True

        self.args = args

        if self["start_index"] < 0:
            raise ValueError("Start index must be a positive integer.")
 
    def set_default(self):
    
        # default parameter value
        self["format"] = "seq:5"
        self["start_index"] = 1
        self["make_new_names"] = False        

def get_formatting_function(format):
    prefix, digits = format.rsplit(":", 1)
    digits = int(digits)
    def formatting_function(index, prefix=prefix, digits=digits):
        return f"{prefix}{str(index).zfill(digits)}"
    return formatting_function

def get_correspondance_table(f):
    '''
    Uses the two first columns of a TAB separated value table to build 
    the correspondance dictionnary.
    '''

    return dict( line.strip().split("\t")[:2] for line in f )

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    
    # make up new names according to the provided format
    if options["make_new_names"]:
        
        # get the formatting function
        format = get_formatting_function(options["format"])

        # write the correspondance table
        with open(options.args.pop(0), "w") as fout:
            sys.argv[1:] = options.args

            # read the input and make new names
            for i, data in enumerate(fasta.reader(fileinput.input()), 
                                     start=options["start_index"]):
                header, sequence = data
                new_header = format(i)
                fasta.writer(new_header, sequence, sys.stdout)
                fout.write(f"{new_header}\t{header}\n")

    # replace names with the name in the table
    else:

        # get conversion table
        with open(options.args.pop(0)) as f:
            d = get_correspondance_table(f)
        sys.argv[1:] = options.args
    
        # read the input and rename sequences
        for header, sequence in fasta.reader(fileinput.input()):
            try:
                new_header = d[header]
            except KeyError:
                new_header = header
            fasta.writer(new_header, sequence, sys.stdout)
    
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())
