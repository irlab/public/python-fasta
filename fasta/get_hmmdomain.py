#!/usr/bin/env python3

'''
USAGE
    fasta_get_hmmdomain.py [OPTION] HMM [FILE...]

DESCRIPTION
    Scan the input sequences with the provided HMM domain using hmmscan
    from the hmmer package and return the sequence corresponding to the
    best domain alignment. Annotate the sequence header with the name
    of the HMM.

    Requires the hmmscan be accessible by the system.

OPTIONS
    -d, --domain=NAME[,...]
        Only output sequences from the provided domain(s).

    -s, --spanning
        Get the sequence ranging from the first to the last domain ^
        found.

    --help
        Display this message

'''

import getopt, sys, fileinput, fasta, subprocess
from tempfile import TemporaryFile

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "d:s", 
                                       ['domain=', 'spanning', 'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-d', '--domain'):
                self["domains"] = a.split(",")
            elif o in ('-s', '--spanning'):
                self["spanning"] = True
        self.args = args
    
    def set_default(self):
    
        # default parameter value
        self["domains"] = None
        self["spanning"] = False

def merge_regions(regions):
    starts, ends = zip(*regions)
    return min(starts), max(ends)

def get_hmm_domain_limits(f, domains=None, merge=False):
    name = model = ""
    c = 0
    rois = {}
    for line in f:

        # detect output for a new query sequence
        if line.startswith("Query:"):
            name = line.split(" ", 1)[1].rsplit(" ", 1)[0].strip()
            continue

        # detect output for a new domain, in that query sequence
        if line.startswith(">>"):
            model = line.split()[1]
            c = 0
            if domains is not None and model not in domains:
                model = ""
            continue

        # record entries for a given query
        if name and model:
            c += 1

            # end for a given query
            if line.startswith("Internal"):
            
                # output
                if merge:
                    for model in rois:
                        start, end = merge_regions(rois[model])
                        yield name, model, start, end
                else:
                    for model in rois:
                        for start, end in rois[model]:
                            yield name, model, start, end

                # re-init values
                name = model = ""
                c = 0
                rois = {}
                continue
        
            # read domain information
            elif c >= 3 and line.strip():
                start = int(line[59:66].strip())-1
                end = int(line[67:74].strip())
                try:
                    rois[model].append((start, end))
                except KeyError:
                    rois[model] = [(start, end)]

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    hmm = options.args.pop(0)
    sys.argv[1:] = options.args
  
    # organize the main job...
    args = ["hmmscan", "--noali", "--notextw", hmm, "-"]
    with TemporaryFile(mode="w+") as in_tmp:

        # get the sequence from the input and copy them in the temporary 
        # file
        sequences = dict()
        for name, sequence in fasta.reader(fileinput.input()):
            sequence = sequence.replace("-", "")
            fasta.writer(name, sequence, in_tmp)
            sequences[name] = sequence
        in_tmp.seek(0)

        # read the sequence with hmmscan and extract domains
        with subprocess.Popen(args, 
                              stdin=in_tmp, 
                              stdout=subprocess.PIPE,
                              encoding="utf-8") as p:
            for name, model, start, end in get_hmm_domain_limits(p.stdout,
                                                                 options["domains"],
                                                                 options["spanning"]):
                sequence = sequences[name][start:end]
                fasta.writer(f"{name}:{model}", sequence, sys.stdout)

    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())

