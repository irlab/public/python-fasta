#!/usr/bin/env python3

'''
USAGE
    fasta_tree_order.py [OPTION] TREE [FILE...]

DESCRIPTION
    Reorder the sequences from the input file according to the order 
    found in the provided TREE file, in Newick format.

OPTIONS
    --help
        Display this message

'''

import getopt, sys, fileinput, fasta, re

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "", ['help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)

        self.args = args
    
    def set_default(self):
    
        # default parameter value
        pass
    
def read_tree_labels(f):
    results = []
    p = re.compile("(?<=[(,])[^:,()]+")
    for line in f:
        results += p.findall(line)
    return results

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)

    # get Newick tree file name
    tree_fname = options.args.pop(0)
    sys.argv[1:] = options.args
    
    # get tree labels in the display order
    with open(tree_fname) as f:
        tree_labels = read_tree_labels(f)

    # store all the input sequences in a dict object
    sequences = dict(fasta.reader(fileinput.input()))
    
    # output sequence in the order of the tree labels
    for name in tree_labels:
        fasta.writer(name, sequences[name], sys.stdout)
    
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())
