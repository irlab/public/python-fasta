#!/usr/bin/env python3

'''
USAGE
    fasta_trim_cds.py [OPTION] [FILE...]

DESCRIPTION
    Trim a coding sequence or an amino acid sequence after the first 
    found stop codon. In amino acid sequences, '*' is recognized as a
    stop codon.

OPTIONS
    -a, --aa
        The provided sequence is an amino acid sequence.

    --help
        Display this message

'''

import getopt, sys, fileinput, fasta

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "a", ['aa', 'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-a', '--aa'):
                self["aa"] = True

        self.args = args
    
    def set_default(self):
    
        # default parameter value
        self["aa"] = False

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args
    
    if options['aa']:
        def find_stop(s):
            return s.find('*')
    else:
        def find_stop(s):
            for i in range(0, len(s), 3):
                if s[i:i+3].upper() in ('TAA', 'TAG', 'TGA'):
                    return i

    # read input sequences
    for name, sequence in fasta.reader(fileinput.input()):
        i = find_stop(sequence)
        if i == -1:
            fasta.writer(name, sequence, sys.stdout)
        else:
            fasta.writer(name, sequence[:i], sys.stdout)
        
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())
