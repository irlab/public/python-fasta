#!/usr/bin/env python3

'''
USAGE
    fasta_reorder [OPTION] [FILE...]

DESCRIPTION
    Reorders the sequence in the input file.

OPTIONS
    -l, --list=FILE
        Reorder according to the list (non-matching entry will be 
        ignored). Without this option, performs alphanumeric sorting.
    
    --help
        Display this message

'''

import getopt, sys, fileinput, fasta
from .codes import ambiguous_nucl

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "l:", 
                                       ['list=', 
                                        'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-l', '--list'):
                self["list"] = a

        self.args = args

    def set_default(self):
    
        # default parameter value
        self["list"] = None

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args

    # load sequences
    sequences = dict(fasta.reader(fileinput.input()))

    # load name list if provided
    if options["list"] is not None:
        with open(options["list"]) as f:
            sorted_names = [ line.strip for line in f ]
    else:
        sorted_names = sorted(sequences.keys())

    # sort sequences
    for name in sorted_names:
        fasta.writer(name, sequences[name], sys.stdout)
        
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())

