# function that are imported upon the command 'import fasta'
from .io import reader
from .io import writer
from .io import is_range
from .io import read_range
from .io import read_ranges