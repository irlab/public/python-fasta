#!/usr/bin/env python3

'''
USAGE
    fasta_alignment_clean [OPTION] [FILE...]

DESCRIPTION
    Discard sites with gaps or ambiguous positions.

OPTIONS
    -c, --codons
        Inform that the sequences are coding, therefore reads the sites
        three-by-three and account for any issue within each supposed 
        codon. If set, the data type will automatically switch to nucl.

    -d, --data-type=nucl|aa
        Data type, which influences the interpretation of ambiguous 
        sites. N or IUPAC degenerate nucletides in nucleotidic 
        sequences, X in amino acid sequences. Default=nucl.
    
    -g, --gap-tolerance=0-1
        Accepted proportion of gaps in a column. Default=0.

    -i, --tolerate-iupac
        In nucleotidic sequences, accepts IUPAC degenerate nucleotides.
        Otherwise, the program deals with them as Ns.     

    -n, --n-tolerance=0-1
        Accepted proportion of ambiguous sites in a column. Default=0.
    
    --help
        Display this message

'''

import getopt, sys, fileinput, fasta
from .codes import ambiguous_nucl

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "cd:g:in:", 
                                       ['codons', 
                                        'data-type=',
                                        'gap-tolerance=',
                                        'tolerate-iupac',
                                        'n-tolerance=',
                                        'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-c', '--codons'):
                self["codons"] = True
            elif o in ('-d', '--data-type'):
                self["data_type"] = a
            elif o in ('-g', '--gap-tolerance'):
                self["gap_tolerance"] = float(a)
            elif o in ('-i', '--tolerate-iupac'):
                self["tolerate_iupac"] = True
            elif o in ('-n', '--n-tolerance'):
                self["n_tolerance"] = float(a)
                    
        
        if self["codons"]:
            self["data_type"] = "nucl"  
        
        self.args = args

    def set_default(self):
    
        # default parameter value
        self["codons"] = False
        self["data_type"] = "nucl"
        self["gap_tolerance"] = 0
        self["tolerate_iupac"] = False
        self["n_tolerance"] = 0

def check_site(site, tolerance, values):
    N = len(site)
    n = len([ x for x in site if x.upper() in values ])
    return n/N <= tolerance

def get_sites(msa):
    for site in zip(*(sequence for _, sequence in msa)):
        yield site

def get_codons(msa):
    position = 0
    codon = []
    for site in zip(*(sequence for _, sequence in msa)):
        codon.append(site)
        if position == 2:
            yield codon
            codon = []
            position = 0
        else:
            position += 1

def filter_sites(sites, tolerance, values):
    return [ site for site in sites if check_site(site, tolerance, values) ]

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args

    # load sequences
    msa = list(fasta.reader(fileinput.input()))
    names = [ name for name, _ in msa ]

    # ambigous position
    if options["data_type"] == "aa":
        amb = "X"
    elif options["data_type"] == "nucl":
        if options["tolerate_iupac"]:
            amb = "N?"
        else:
            amb = "".join(ambiguous_nucl.keys())
    else:
        sys.stderr.write(f"unknown data type: {options['data_type']}\n")
        return 1

    # filter by codon
    if options["codons"] == True:
        filtered_sites = []
        for i, codon in enumerate(get_codons(msa)):

            # gap filtering
            if len(filter_sites(codon, options["gap_tolerance"], "-")) < 3:
                continue

            # N filtering
            if len(filter_sites(codon, options["n_tolerance"], amb)) < 3:
                continue
            
            # keep sites that passed the filtering
            codon = [ "".join(sites) for sites in zip(*codon) ]
            filtered_sites.append(codon)
    
    # filter by site
    else:
        sites = get_sites(msa)
        
        # gap filtering
        filtered_sites = filter_sites(sites, options["gap_tolerance"], "-")

        # N filtering
        filtered_sites = filter_sites(filtered_sites, options["gap_tolerance"], amb)

    # output the filtered sites
    for name, sequence in zip(names, zip(*filtered_sites)):
        fasta.writer(name, "".join(sequence), sys.stdout)
    
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())

