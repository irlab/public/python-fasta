#!/usr/bin/env python3

'''
USAGE
    fasta_filter [OPTION] [LIST] [FILE...]

DESCRIPTION
    Filter a FASTA files with a given list of sequence headers.
    
OPTIONS
    -p, --pattern=REGEX
        Filter sequence names using the string matching the provided 
        regular expression instead of the whole name.
        
    -s, --sequences=FILE
        Provide the sequence file with this option and read the list 
        from the standard input.
        
    -v, --inverse
        Output sequences that do not match the list names.

    --help
        Display this message

'''

import getopt, sys, fileinput, fasta, re

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "p:s:v", 
                                       ['pattern=', 'sequences=',
                                        'inverse', 'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-p', '--pattern'):
                self['pattern'] = re.compile(a)
            elif o in ('-s', "--sequences"):
                self["sequences"] = a
            elif o in ('-v', '--inverse'):
                self["inverse"] = True

        self.args = args
    
    def set_default(self):
    
        # default parameter value
        self["pattern"] = None
        self["sequences"] = None
        self["inverse"] = False

def get_get(pattern):
    if pattern is not None:
        def get(x):
            try:
                return pattern.search(x).group()
            except AttributeError:
                sys.stderr.write(f'The provided pattern {repr(pattern.pattern)}'
                                  ' was not found in the input name:'
                                 f' {repr(x)}\n')
            return ""
    else:
        def get(x):
            return x
    return get

def get_list(f, pattern=None):
    get = get_get(pattern)
    return { get(line.strip()) for line in f }

def filter_sequences(f, names, inverse=False, pattern=None):
    get = get_get(pattern)

    if not inverse:
        def filter_out(x, names=names):
            return x not in names
    else:
        def filter_out(x, names=names):
            return x in names
    for header, sequence in fasta.reader(f):
        x = get(header)
        if not x: return 1
        if filter_out(x): continue
        fasta.writer(header, sequence, sys.stdout)
    return 0

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    
    # get conversion table and filter the sequences
    if options["sequences"] is None:
        with open(options.args.pop(0)) as f:
            s = get_list(f, pattern=options["pattern"])
        sys.argv[1:] = options.args
        r = filter_sequences(fileinput.input(), s, 
                         inverse=options["inverse"], 
                         pattern=options["pattern"])
    else:
        sys.argv[1:] = options.args
        s = get_list(fileinput.input())
        with open(options["sequences"]) as f:
            r = filter_sequences(f, s, 
                             inverse=options["inverse"], 
                             pattern=options["pattern"])
    
    # return 0 if everything succeeded
    return r

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())

