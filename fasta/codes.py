#!/usr/bin/env python3

ambiguous_nucl = {
    "R": set("AG"),
    "Y": set("CT"),
    "S": set("GC"),
    "W": set("AT"),
    "K": set("GT"),
    "M": set("AC"),
    "B": set("CGT"),
    "D": set("AGT"),
    "H": set("ACT"),
    "V": set("ACG"),
    "N": set("ATGC"),
    "?": set("ATGC")
}

class EukariotGeneticCode(dict):
    '''translate codon into amino acid according eukariotic genetic code'''
    
    amb = ambiguous_nucl

    def __init__(self, value={}, allow_ambiguous=False):
        dict.__init__(value)
        codons = []
        for first in ('T','C','A','G') :
            for second in ('T','C','A','G') :
                for third in ('T','C','A','G'):
                    codons += [first + second + third]
        AA = ['F','F','L','L','S','S','S','S',
              'Y','Y','*','*','C','C','*','W',
              'L','L','L','L','P','P','P','P',
              'H','H','Q','Q','R','R','R','R',
              'I','I','I','M','T','T','T','T',
              'N','N','K','K','S','S','R','R',
              'V','V','V','V','A','A','A','A',
              'D','D','E','E','G','G','G','G']
        self.update(dict(zip(codons, AA)))
        self.update(value)
        self.allow_ambiguous=allow_ambiguous
    
    def possible_aas(self, codon):
        codon = codon.upper()
        sites = []
        for base in codon:
            try:
                sites.append(self.amb[base])
            except KeyError:
                sites.append(set(base))
        return { dict.__getitem__(self, "".join((x, y, z))) 
                    for x in sites[0] 
                    for y in sites[1] 
                    for z in sites[2] }
    
    def __getitem__(self, codon):
        if codon == "---":
            return "-"
        if "-" in codon or len(codon) < 3:
             raise KeyError(f"incomplete codon: {repr(codon)}")
        aas = self.possible_aas(codon)
        if len(aas) > 1:
            if self.allow_ambiguous:
                return "X"
            else:
                raise ValueError(f"ambiguous codon: {repr(codon)}")
        else:
            return aas.pop() 

class ReverseComplement(object):
    def __init__(self) -> None:
        self._table = str.maketrans("garkbdctymvhuGARKBDCTYMVHU", 
                                    "ctymvhgarkbdaCTYMVHGARKBDA")

    @property
    def table(self):
        return self._table
    
    def reverse_complement(self, sequence):
        return sequence[::-1].translate(self.table)
