#!/usr/bin/env python3

'''
USAGE
    fasta_join_alignments [OPTION] FILE1 FILE2[...]

DESCRIPTION
    Join sequences from several alignments files.

OPTIONS
    -d, --delim=STR
        Add the provided string in between the joined sequences.
    
    -n, --nexus-partition=FILE
        Write partitions in Nexus format, in the FILE.

    -p, --pattern=REGEX
        Detect a common sequence identifier in the input sequence 
        headers using the provided regular expression. The matched 
        strings will be use as sequence headers in the output file.
    
    --help
        Display this message

'''

import getopt, sys, fasta, re

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "d:n:p:", 
                                       ['delim=', 'nexus=', 'pattern=', 'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-d', '--delim'):
                self["delim"] = a
            elif o in ('-n', '--nexus'):
                self["nexus"] = a    
            elif o in ('-p', '--pattern'):
                self["pattern"] = re.compile(a)

        self.args = args
    
    def set_default(self):
    
        # default parameter value
        self["delim"] = ""
        self["nexus"] = None
        self["pattern"] = re.compile(".+")

def get_sequences(f, pattern):
    for name, sequence in fasta.reader(f):
        try:
            name = pattern.search(name).group()
        except ValueError:
            raise ValueError(f"The provided identifier pattern "
                             f"{pattern.pattern} was not found in this "
                             f"sequence header: {name}")
        yield name, sequence

def write_nexus(partitions, fout):

    # code block start
    fout.write('#nexus\nbegin sets;\n')

    # partition lines
    for i, (start, end) in enumerate(partitions):
        fout.write(f"  charset part{i+1} = {start}-{end};\n")

    # code block end
    fout.write('end;\n')

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args
    
    # get input sequences
    sequences = dict()
    partitions = []
    for fname in options.args:
        with open(fname) as f:
            for name, sequence in get_sequences(f, options["pattern"]):
                try:
                    sequences[name] += (options["delim"] + sequence)
                except KeyError:
                    sequences[name] = sequence
        try:
            start = partitions[-1][-1] + len(options["delim"]) + 1
        except IndexError:
            start = 1
        end = start + len(sequence)-1
        partitions.append((start, end))

    # exit if no sequences were read from the input
    if len(sequences) == 0: return 0

    # check that all sequences have the same length
    l = len(list(sequences.values())[0])
    if any( len(sequence) != l for sequence in sequences.values() ):
        raise ValueError("output sequences do not have the same length")

    # write the output(s)
    for name in sequences:
        fasta.writer(name, sequences[name], sys.stdout)
    if options["nexus"] is not None:
        with open(options["nexus"], "w") as fout:
            write_nexus(partitions, fout)
    
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())

