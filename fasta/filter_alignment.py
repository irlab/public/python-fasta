#!/usr/bin/env python3

'''
USAGE
    fasta_filter_aligment.py [OPTION] N-M[,N-M...] [FILE...]

DESCRIPTION
    Filter sequences that contain mutations in the provided range(s)
    N-M. N and M are 1-base closed interval coordinates. If motives are
    provided along with the ranges (option -m), the sequences must 
    comply to the provided variants with a given degree of conservation 
    (options -c). Otherwise, the program just look for gaps in the
    ranges.

OPTIONS
    -c, --conservation=FLOAT
        Minimum conservation level (rate of sites complying to the motif 
        or with no gaps, when no motif were provided) with respect of 
        all provided ranges. Default=1 (no mutation).

    -d, --max-differences=INT
        Instead of a minimum conservation level, set the maximum number
        of mutations allowed. Default=0 (no mutation).

    -m, --motives=MOTIF[,...]
        Expected motives of the ranges. Sites are separated by :, 
        sites with all values accepted except gaps are noted "X", sites
        with all values accepted including gaps are noted "-".
    
    --help
        Display this message

'''

import getopt, sys, fileinput, fasta

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "c:d:m:", 
                                       ['conservation=', 
                                        'max-differences=',
                                        'motives=',
                                        'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-c', '--conservation'):
                self["conservation"] = float(a)
            elif o in ('-d', '--max-differences'):
                self["max_differences"] = int(a)
            elif o in ('-m', '--motives'):
                self["motives"] = read_motives(a)
        
        self["slices"] = read_ranges(args[0])
        self.args = args[1:]    
        
    def set_default(self):
    
        # default parameter value
        self["slices"] = []
        self["motives"] = []
        self["conservation"] = 1
        self["max_differences"] = 0

def read_ranges(s):
    ranges = [ map(int, x.split("-")) for x in s.split(",") ]
    return [ slice(i-1, j) for i, j in ranges ]

def read_motives(s):
    return [ x.split(":") for x in s.lower().split(",") ]

def conserved_position(s, motif):
    c = 0
    for value, pattern in zip(s, motif):
        if pattern == "-":
            continue
        if pattern == "x" and value != "-":
            continue
        if value not in pattern:
            c += 1
    return c

def compile_ranges_and_motives(ranges, motives):
    compiled = []
    for i in range(len(ranges)):
        s = ranges.pop(0)
        l = (s.stop - s.start)
        try:
            m = motives.pop(0)
        except IndexError:
            m = [""]
        if m == [""]:
            m = ["x"] * l
        compiled.append((s, l, m))
    return compiled

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args

    # compile ranges and motives
    ranges_and_motives = compile_ranges_and_motives(
        options["slices"], 
        options["motives"])
    
    # The score is the raw number of differences
    if options["max_differences"] > 0:
        def keep(c, l, threshold=options["max_differences"]):
            return c <= threshold
    
    # The score is the number of differences / sequence length
    elif options["conservation"] < 1:
        def keep(c, l, threshold=options["conservation"]):
            return (1-(c/l)) >= threshold
    
    # The score is the raw number of differences, no difference is 
    # allowed
    else:
        def keep(c, l):
            return c == 0

    # organize the main job...
    for name, sequence in fasta.reader(fileinput.input()):
        C, L = 0, 0
        for s, l, m in ranges_and_motives:
            C += conserved_position(sequence[s].lower(), m)
            L += l
        if keep(C, L):
            fasta.writer(name, sequence, sys.stdout)
    
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())

