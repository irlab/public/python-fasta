#!/usr/bin/env python3

'''
USAGE
    fasta_remove_gaps.py [OPTIONS] [FILE...]

DESCRIPTION
    Remove sites with more than X percent of gaps in an alignment.

OPTIONS
    -p, --percent=X
        Define the maximum percentage of gaps in a site to be kept. 
        Default = 100 (sites composed of gaps only are removed).
    
    --help
        Display this message

'''

import getopt, sys, fileinput, fasta

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "p:", ['percent=', 'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-p', '--percent'):
                self["percent"] = float(a)
        
        self.args = args
        
    def set_default(self):
    
        # default parameter value
        self["percent"] = 100

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args
    
    # organize the main job...
    alignment = [ (name, sequence) 
                  for name, sequence in fasta.reader(fileinput.input()) ]
    
    # exit if no sequences were provided
    if not alignment: return 0 

    # find the alignment length
    l = len(alignment[0][1])
    n = len(alignment)
    
    # read the alignment site by site
    site_to_keep = []
    for i in range(l):
        site = []
        for name, sequence in alignment:
            site.append(sequence[i])
        gap_number = site.count("-")
        percent_gap = (gap_number/n)*100
        if percent_gap < options["percent"]:
            site_to_keep.append(i)
    
    for name, sequence in alignment:
        new_sequence = "".join([ sequence[i] for i in range(l) if i in site_to_keep ])
        fasta.writer(name, new_sequence, sys.stdout)
        
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())

