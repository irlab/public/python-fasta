#!/usr/bin/env python3

'''
USAGE
    fasta_find_orfs.py [OPTION] [FILE...]

DESCRIPTION
    Find every possible ORF in the provided nucleotide sequences.
    
OPTIONS
    -l, --longest
        Only output the longest ORF(s). Print a warning if there is 
        several longest ORF in one sequence. 

    -s, --no-stop
        Includes non-terminated ORFs.
        
    -x, --allow-ambiguous
        Codons that are not included in the genetic code (i.e. with 
        IUPAC ambiguous nucleotides) are translated into X.

    --help
        Display this message

'''

import getopt, sys, fileinput, fasta
from .codes import EukariotGeneticCode

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "lsx", 
                                       ['longest', 
                                        'no-stop',
                                        'allow-ambiguous', 
                                        'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-l', '--longest'):
                self["longest"] = True
            elif o in ('-s', '--no-stop'):
                self["no_stop"] = True
            elif o in ('-x', '--allow-ambiguous'):
                self["allow_ambiguous"] = True

        self.args = args
    
    def set_default(self):
    
        # default parameter value
        self["longest"] = False
        self["no_stop"] = False
        self["allow_ambiguous"] = False

def get_orfs(sequence, gc, no_stop=False):
    '''
    Get ORF coordinates and sequences the provided sequence
    '''

    for frame in (0, 1, 2):
        max_len = len(sequence)-(len(sequence)-frame)%3
        i = frame
        while i < max_len:
            if gc[sequence[i:i+3]] == "M":
                terminated = False
                j = i+3
                for j in range(i+3, max_len, 3):
                    if gc[sequence[j:j+3]] == "*":
                        yield (i, j+3, sequence[i:j+3])
                        terminated = True
                        break
                if (not terminated) and no_stop:
                    yield (i, max_len, sequence[i:max_len])
                i = j+3
            i+=3

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args
    
    # initialize the genetic code
    gc = EukariotGeneticCode(allow_ambiguous=options["allow_ambiguous"])

    # read input sequences    
    for name, sequence in fasta.reader(fileinput.input()):

        # generator instance
        orfs = get_orfs(sequence, gc, no_stop=options["no_stop"])

        # returns only the longest ORFs
        if options["longest"]:
            start, end, orf_sequence = sorted(orfs, 
                                              key=lambda data: data[1]-data[0], 
                                              reverse=True)[0]
            fasta.writer(f"{name}:{start}:{end}", orf_sequence, sys.stdout)
        else:
            for start, end, orf_sequence in orfs:
                fasta.writer(f"{name}:{start}:{end}", orf_sequence, sys.stdout)

    # return 0 if everything succeeded
    return 0

if __name__ == "__main__":
    sys.exit(main())
