#!/usr/bin/env python3

'''
USAGE
    fasta_len_filter.py [OPTION] [FILE...]

DESCRIPTION
    Filter sequences by length.

OPTIONS
    -m, --min-len=INT
        Minimum sequence length.

    -M, --max-len=INT
        Maximum sequence length.

    --help
        Display this message

'''

import getopt, sys, fileinput, fasta

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "m:M:", 
                ['min-len=', 'max-len=', 'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-m', '--min-len'):
                self["min_len"] = int(a)
            elif o in ('-M', '--max-len'):
                self["max_len"] = int(a)

        self.args = args
    
    def set_default(self):
    
        # default parameter value
        self["min_len"] = 0
        self["max_len"] = None
    
def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args
    
    # organize the main job...
    
    # parse input
    for header, sequence in fasta.reader(fileinput.input()):
        
        # evaluate the minimum length
        if len(sequence) < options["min_len"]:
            continue
            
        # evaluate the maximum length
        if options["max_len"] is not None and len(sequence) > options["max_len"]:
            continue

        fasta.writer(header, sequence, sys.stdout)
        
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())
