#!/usr/bin/env python3

'''
USAGE
    fasta_sliding_windows.py LENGHT STEP [FILE...]

DESCRIPTION
    Generate a fasta file with sequences extracted from a sliding 
    window defined with the input parameters LENGTH (for the length of
    the window) and STEP (for the step of the window). The window range
    is added to the sequence header in 1-based closed interval notations.

OPTIONS
    --help
        Display this message

'''

import getopt, sys, fileinput, fasta

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "", ['help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)

        self.args = args
    
    def set_default(self):
    
        # default parameter value
        pass

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    length = int(options.args.pop(0))
    step =int(options.args.pop(0))
    sys.argv[1:] = options.args

    # read input sequences
    for name, sequence in fasta.reader(fileinput.input()):
        for i in range(0, len(sequence), step):
            start = i+1
            end = (i+length) if (i+length < len(sequence)) else len(sequence)
            sw_name = f"{name}:{start}-{end}"
            sw_sequence = sequence[i:end]
            fasta.writer(sw_name, sw_sequence, sys.stdout)
        
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())
