#!/usr/bin/env python3

'''
USAGE
    fasta_translate.py [OPTION] [FILE...]

DESCRIPTION
    Translate nucleotidic sequences into amino acid sequences.
    
OPTIONS
    -t, --trim
        Trim input sequences if the length is not a multiple of three.

    -x, --allow-ambiguous
        Codons that are not included in the genetic code (i.e. with 
        IUPAC ambiguous nucleotides) are translated into X.

    --help
        Display this message

'''

import getopt, sys, fileinput, fasta
from .codes import EukariotGeneticCode

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "tx", 
                                       ['trim', 'allow-ambiguous', 
                                        'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-t', '--trim'):
                self["trim"] = True
            elif o in ('-x', '--allow-ambiguous'):
                self["allow_ambiguous"] = True

        self.args = args
    
    def set_default(self):
    
        # default parameter value
        self["trim"] = False
        self["allow_ambiguous"] = False

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args
    
    # initialize the genetic code
    gc = EukariotGeneticCode(allow_ambiguous=options["allow_ambiguous"])

    # input sequence preparation
    if options["trim"]:
        def prepare(sequence):
            excess = len(sequence) % 3
            if excess > 0:
                return sequence[:-excess]
            else:
                return sequence
    else:
        def prepare(sequence):
            return sequence
    
    # compile the translator
    def translate(sequence, prepare=prepare, gc=gc):
        sequence = prepare(sequence)
        return "".join( gc[sequence[i:i+3]]
                        for i in range(0, len(sequence), 3))
    
    for name, sequence in fasta.reader(fileinput.input()):
        try:
            fasta.writer(name, translate(sequence.upper()), sys.stdout)
        except KeyError as e:
            if "incomplete codon" in str(e):
                sys.stderr.write("Interrupted translation:\n"
                                 f"   {e} in sequence {repr(name)}\n")
                return 1
            raise
        except ValueError as e:
            if "ambiguous codon" in str(e):
                sys.stderr.write("Interrupted translation:\n"
                                 f"   {e} in sequence {repr(name)}\n")
                return 1
            raise

    # return 0 if everything succeeded
    return 0

if __name__ == "__main__":
    sys.exit(main())
