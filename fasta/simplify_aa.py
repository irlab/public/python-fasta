#!/usr/bin/env python3

'''
USAGE
    fasta_simplify_aa.py [OPTION] [FILE...]

DESCRIPTION
    Convert an amino-acid sequence into a sequence of amino-acid 
    classes based on Margaret Dayhoff's scheme:

    Amino acid      Property                Dayhoff
    -------------   ---------------------   -------
    C               Sulfur polymerization   a
    G, S, T, A, P   Small                   b
    D, E, N, Q      Acid and amide          c
    R, H, K         Basic                   d
    L, V, M, I      Hydrophobic             e
    Y, F, W         Aromatic                f 

OPTIONS
    --help
        Display this message

'''

import getopt, sys, fileinput, fasta

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "", ['help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)

        self.args = args
    
    def set_default(self):
    
        # default parameter value
        pass

class Dayhoff(object):
    table = str.maketrans(
        "C" "GSTAP" "DENQ" "RHK" "LVMI" "YFW",
        "a" "bbbbb" "cccc" "ddd" "eeee" "fff"
    )

    def __init__(self) -> None:
        pass
    
    def convert(self, s):
        return s.upper().translate(self.table)


def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args
    translator = Dayhoff()

    # organize the main job...
    for header, sequence in fasta.reader(fileinput.input()):
        fasta.writer(header, translator.convert(sequence), sys.stdout)
    
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())

