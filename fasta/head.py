#!/usr/bin/env python3

'''
USAGE
    fasta_head.py [OPTION] [FILE...]

DESCRIPTION
    Extract the first N sequences from a FASTA file.
    
OPTIONS
    -n, --number=N
        Number of sequences to be extracted. Default = 10.
        
    --help
        Display this message

'''

import getopt, sys, fileinput, fasta

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "n:", ['number=', 'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-n', '--number'):
                self["n"] = int(a)

        self.args = args
    
    def set_default(self):
    
        # default parameter value
        self["n"] = 10

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
        
    sys.argv[1:] = options.args
    
    # counting variable
    c = 0
    for header, sequence in fasta.reader(fileinput.input()):
        fasta.writer(header, sequence, sys.stdout)
        c += 1
        if c == options["n"]:
            break
    
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())

