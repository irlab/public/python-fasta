#!/usr/bin/env python3

'''
USAGE
    fasta_join_files.py [OPTIONS] FILE...

DESCRIPTION
    Use this program to merge fasta files, adding file names to 
    corresponding sequence names.

OPTIONS
    -d, --delim=STR
        Set the delimiter to be inserted between the file name and the
        sequence name. Default=":"
    
    -r, --remove=PATTERN
        Remove the string matched by the regular expression PATTERN in 
        the file name.

    --help
        Display this message

'''

import getopt, sys, re, fasta

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], 
                "d:r:",   
                ['delim=', 'remove=', 'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-d', '--delim'):
                self["delim"] = a
            elif o in ('-r', '--remove'):
                self["remove"] = re.compile(a)

        self.args = args
    
    def set_default(self):
    
        # default parameter value
        self["delim"] = ":"
        self["remove"] = None

def get_remove_func(pattern):
    if pattern is None:
        def remove(s):
            return s
    else:
        def remove(s):
            pattern.sub("", s)
    return remove

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args
    delim = options["delim"]
    remove = get_remove_func(options["remove"])

    # read each input file
    while sys.argv[1:]:
        fname = sys.argv.pop(1)
        with open(fname) as f:
            for name, sequence in fasta.reader(f):
                fasta.writer( 
                    f"{remove(fname)}{delim}{name}",
                    sequence,
                    sys.stdout)
    
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())

