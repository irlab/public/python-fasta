#!/usr/bin/env python3

'''
USAGE
    fasta_split.py [-f RANGE | -p PATTERN ] [OPTION] [FILE...]

DESCRIPTION
    Split input sequences into different files. Sort sequences using a 
    pattern matching segregating information in the sequence headers. 
    Files are named using the matching string and adding the .fas 
    extension.
    
OPTIONS
    -d, --delim=STR
        Field delimiter if option -f is selected.
    
    -f, --fields=RANGE
        Given that the sequence headers would contains delimiter 
        characters (default "|", parametrable with the -d option), 
        designate fields by their index that should constitute the 
        segregating information. The indexes are specified by a 1-base 
        closed RANGE In the file name, fields are replaced by "_". This
        option overrides the -p option.

    -k, --keep-names
        Segregating information is not removed from the sequence 
        headers.

    -p, --pattern=STR
        Use a regular expression pattern to match the segregating 
        information in the sequence headers. This option is ignore if -f
        is specified.
    
    -u, --unmatched
        Write unmatched sequences in the standard output
       
    --help
        Display this message

'''

import getopt, os, sys, fileinput, fasta, re
from functools import partial

class Options(dict):

    def __init__(self, argv):
        
        # set default
        self.set_default()
        
        # handle options with getopt
        try:
            opts, args = getopt.getopt(argv[1:], "d:f:kp:u", 
                                       ['delim=',
                                        'fields=',
                                        'keep-names',
                                        'pattern=',
                                        'unmatched',
                                        'help'])
        except getopt.GetoptError as e:
            sys.stderr.write(str(e) + '\n' + __doc__)
            sys.exit(1)

        for o, a in opts:
            if o == '--help':
                sys.stdout.write(__doc__)
                sys.exit(0)
            elif o in ('-d', '--delim'):
                self['delim'] = a
            elif o in ('-f', '--fields'):
                self['fields'] = fasta.read_range(a)
            elif o in ('-k', '--keep-names'):
                self['keep_names'] = True
            elif o in ('-p', '--pattern'):
                self['pattern'] = re.compile(a)
            elif o in ('-u', '--unmatched'):
                self['unmatched'] = True

        self.args = args

    def set_default(self):
    
        # default parameter value
        self["delim"] = "|"
        self["fields"] = None
        self["keep_names"] = False
        self["pattern"] = None
        self["unmatched"] = False

def get_fields(name, field_range, delim="|", keep_in_name=False):
    fields = name.split(delim)
    group = [ field for i, field in enumerate(fields) if i+1 in field_range ]
    if not keep_in_name:
        name = delim.join( field for field in fields if not field in group )
    group = "_".join(group) if group else "unmatched"
    return (group, name)

def get_match(name, pattern, keep_in_name=False):
    m = pattern.search(name)
    if m is None:
        return ("", name)
    group = m.group()
    start, end = m.span()
    if not keep_in_name:
        name = name[:start] + name[end:]
    if group == "":
        group = "unmatched"
    return (group, name)

def main(argv=sys.argv):
    
    # read options and remove options strings from argv (avoid option 
    # names and arguments to be handled as file names by
    # fileinput.input().
    options = Options(argv)
    sys.argv[1:] = options.args
    
    if options["fields"] is not None:
        get_group = partial(get_fields, 
                            field_range=options["fields"],
                            delim=options["delim"], 
                            keep_in_name=options["keep_names"])
    else:
        if options["pattern"] is None:
            sys.stderr.write("Error: either option -f or -p must be defined.\n")
            return 1
        get_group = partial(get_match,
                            pattern=options["pattern"],
                            keep_in_name=options["keep_names"])

    # sequences by group
    records = dict(unmatched=[])

    # search and count patterns in input sequences
    for name, sequence in fasta.reader(fileinput.input()):
        
        # get the segregating information
        group, name = get_group(name)
        try:
            records[group].append((name, sequence))
        except KeyError:
            records[group] = [(name, sequence)]

    # write unmatched sequences in the standard output
    if options["unmatched"]:
        for name, sequence in records["unmatched"]:
            fasta.writer(name, sequence, sys.stdout) 
    del records["unmatched"]

    # write the files
    for group in records:
        with open(f"{group}.fas", "w") as fout:
            for name, sequence in records[group]:
                fasta.writer(name, sequence, fout)
        
    # return 0 if everything succeeded
    return 0

# does not execute main if the script is imported as a module
if __name__ == '__main__': sys.exit(main())

